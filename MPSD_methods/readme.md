Note on cloning submodules
--------------------------

The `methods_paper` and `analysis` directories are git submodules that point to the mpsd_methods_paper and mpsd_methods_analysis repositories on bitbucket/msena.

Upon cloning this repository, these directories will be empty.
To populate them with the contents of their respective remote repositories one must run two commands:  
`git submodule init`  
to initialize your local configuration file, and  
`git submodule update`  
to fetch all the data from that project and check out the appropriate commit listed in your superproject
