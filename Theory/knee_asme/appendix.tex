\subsection{Explicit expressions for the Euler and dual-Euler basis vectors for the 1-2-3 set of Euler angles}\label{app:123-Euler}

	In the paper, we follow \cite{GS:1983} and use a 1-2-3 set of Euler angles.

	For the 1-2-3 set, the rotation $\mathsf{R}$ has the decomposition 
	\begin{equation}
	\mathsf{R} = \mathsf{C}\mathsf{B}\mathsf{A},
	\end{equation}
	where $\mathsf{A}$, $\mathsf{B}$, and $\mathsf{C}$ are three rotation matrices:
	\begin{eqnarray}
	\mathsf{A} &=&
	\left[ \begin{array}{c c c }
	1 &  0 & 0
	 \\ 0 & \cos\left(\psi\right)  &  \sin\left(\psi\right)  \\
	0 & - \sin\left(\psi\right) &  \cos\left(\psi\right)
	\end{array} \right],
	\nonumber \\
	\mathsf{B} &=&
	\left[ \begin{array}{c c c }
	\cos\left(\theta\right) & 0 &  - \sin\left(\theta \right)  \\
	0 & 1 &  0 \\
	 \sin\left(\theta\right)  & 0 &  \cos\left(\theta \right) 
	\end{array} \right],
	\nonumber \\
	\mathsf{C} &=&
	\left[ \begin{array}{c c c }
	\cos\left(\phi\right) &   \sin\left(\phi\right) & 0
	 \\  - \sin\left(\phi\right) &  \cos\left(\phi\right) & 0  \\
	0 & 0 &  1
	\end{array} \right].
	\end{eqnarray}
	
	The most frequently used choice of Euler angles in biomechanics is the 3-2-1 set and the corresponding developments for this set can be found in \cite{OOR06,OOR08}.

	\begin{figure}[t]
		\centering
	  \includegraphics[width=0.7\textwidth]{Theory/figs/dual-cones.pdf}
	  % \includegraphics[width=0.5\textwidth]{Theory/knee_asme/FigureA1-eps-converted-to.pdf}
		% \begin{center} \epsfig{file=FigureA1.eps,scale=0.8} \end{center}
		\caption[Graphical representation of the dual-Euler and Euler basis vectors]
		{Graphical representation of the dual-Euler and Euler basis vectors and their relationships with the proximal $\mathbb{P}$ and distal $\mathbb{D}$ frames.
		Explicit expressions for these vectors can be found in (\ref{euler-basis-1-2-3}).
		In this figure $\theta > 0$.
		}\label{figure-dual-Eulerd}
	\end{figure}


	The Euler basis and dual-Euler basis for the 1-2-3 set of Euler angles can be expressed in terms of the proximal basis vectors and the distal basis vectors:
	
	\begin{eqnarray}\label{euler-basis-1-2-3}
	\left[ \begin{array}{c}
	 {\bf g}_1  \\ {\bf g}_2   \\ {\bf g}_3
	\end{array} \right]
	&=& \mathsf{E}_p \left[\begin{array}{c}
	{\bf p}_1 \\ {\bf p}_2 \\ {\bf p}_3
	\end{array} \right]
	= \mathsf{E}_d \left[\begin{array}{c}
	{\bf d}_1 \\ {\bf d}_2 \\ {\bf d}_3
	\end{array} \right], 
	\nonumber \\
	\left[ \begin{array}{c}
	 {\bf g}^1 \\ {\bf g}^2 \\ {\bf g}^3
	\end{array} \right]
	&=& \mathsf{G}_p \left[\begin{array}{c}
	{\bf p}_1 \\ {\bf p}_2 \\ {\bf p}_3
	\end{array} \right]
	= \mathsf{G}_d \left[\begin{array}{c}
	{\bf d}_1 \\ {\bf d}_2 \\ {\bf d}_3
	\end{array} \right].
	\end{eqnarray}
	%
	The four matrices in these equations have the representations 
	
	\begin{eqnarray}
	\mathsf{E}_p 
	&=&
	\left[ \begin{array}{c c c}
	1 & 0 & 0 \\
	0 & \cos\left(\psi\right) & \sin\left(\psi\right) \\
	\sin\left(\theta\right)  & - \cos\left(\theta\right)\sin\left(\psi\right) & \cos\left(\theta\right)\cos\left(\psi\right) \\
	\end{array} \right],
	\nonumber \\
	\mathsf{E}_d  &=&
	\left[ \begin{array}{c c c}
	  \cos\left(\theta\right)\cos\left(\phi\right) &
	-  \cos\left(\theta\right)\sin\left(\phi\right)  & 
	 \sin\left(\theta\right) \\
	\sin\left(\phi\right) & \cos\left(\phi\right) & 0 \\
	0 & 0 & 1
	\end{array} \right],
	\end{eqnarray}
	%
	and
	%
	\begin{eqnarray}
	\mathsf{G}_p  &=& 
	\left[ \begin{array}{c c c}
	1 & \sin\left(\psi\right)\tan\left(\theta\right) & - \cos\left(\psi\right)\tan\left(\theta\right) \\
	0 & \cos\left(\psi\right) & \sin\left(\psi\right) \\
	0 & -  \sec\left(\theta\right)\sin\left(\psi\right) & \sec\left(\theta\right)\cos\left(\psi\right) \\
	\end{array} \right],
	\nonumber \\
	\mathsf{G}_d  &=&  
	\left[ \begin{array}{c c c}
	 \sec\left(\theta\right)\cos\left(\phi\right)  & \sec\left(\theta\right)\sin\left(\phi\right) &  0\\
	\sin\left(\phi\right) & \cos\left(\phi\right) & 0 \\
	- \tan\left(\theta\right)\cos\left(\phi\right)  &  \tan\left(\theta\right)\sin\left(\phi\right) &  1 \\
	\end{array} \right].
	\end{eqnarray}
	%
	The second Euler angle needs to be restricted to $\theta \in \left(- \frac{\pi}{2}, \frac{\pi}{2} \right)$.
	We also note the identities:
	%
	\begin{displaymath}
	\mathsf{E}_d  = \mathsf{E}_p\mathsf{R}, \qquad
	\mathsf{G}_d \left(\mathsf{E}_d\right)^{-1} = \mathsf{G}_p \left(\mathsf{E}_p\right)^{-1},
	\end{displaymath}
	\begin{equation}\label{GE-ident}
	\mathsf{G}_d  =  \left(\mathsf{E}_d\right)^{-T} = \mathsf{G}_p\mathsf{R}^T, \qquad
	\mathsf{G}_p =  \left(\mathsf{E}_p\right)^{-T} = \mathsf{G}_d \mathsf{R}.
	\end{equation}
	These identities can be used to establish the following relationships:
	\begin{eqnarray}\label{euler-basis-dual-euler-basis}
	\left[
	\begin{array}{c}
	{\bf g}^1 \\ {\bf g}^2 \\ {\bf g}^3
	\end{array}
	\right]
	&=&
	\sec^2\left(\theta\right)
	\left[
	\begin{array}{c c c}
	1 & 0 &  - \sin(\theta)  \\
	0 & \cos^2\left(\theta\right) & 0 \\
	 - \sin(\theta) & 0 & 1
	\end{array}
	\right]
	\left[
	\begin{array}{c}
	{\bf g}_1 \\ {\bf g}_2 \\ {\bf g}_3
	\end{array}
	\right], 
	\nonumber \\
	\left[
	\begin{array}{c}
	{\bf g}_1 \\ {\bf g}_2 \\ {\bf g}_3
	\end{array}
	\right]
	&=&
	\left[
	\begin{array}{c c c}
	1 & 0 &  \sin(\theta)  \\
	0 & 1 & 0 \\
	 \sin(\theta) & 0 & 1
	\end{array}
	\right]
	\left[
	\begin{array}{c}
	{\bf g}^1 \\ {\bf g}^2 \\ {\bf g}^3
	\end{array}
	\right].
	\end{eqnarray}
	%
	To illuminate the relations (\ref{euler-basis-1-2-3}), it is convenient to consider graphical representations of the various basis vectors.
	These representations, first with respect to the distal basis, and then with respect to the proximal basis are shown in Figure \ref{figure-dual-Eulerd}, respectively.
	Referring to Figure \ref{figure-dual-Eulerd}, we observe a pair of cones of semi-angle $\theta$ whose axes of symmetry are defined by ${\bf g}_1$ and ${\bf g}_3$, respectively.
	For a fixed value of $\theta$ the cones can be considered to spin ($\psi$) and precess ($\phi$).

	% With the help of (\ref{euler-basis-1-2-3}) and (\ref{euler-basis-dual-euler-basis}), we can express (\ref{trans-basis}) and (\ref{trans-dual-basis}) in several convenient representations:
	% %
	% \begin{eqnarray}\label{disp-basis}
	% \left[
	% \begin{array}{c}
	% {\bf a}_1 \\ {\bf a}_2 \\ {\bf a}_3
	% \end{array}
	% \right]
	% &=&
	% {\sf TE}_p\left[
	% \begin{array}{c}
	% {\bf p}_1 \\ {\bf p}_2 \\ {\bf p}_3
	% \end{array}
	% \right] = 
	% {\sf TE}_d\left[
	% \begin{array}{c}
	% {\bf d}_1 \\ {\bf d}_2 \\ {\bf d}_3
	% \end{array}
	% \right]
	% \nonumber \\
	% &=& 
	% \left[
	% \begin{array}{c c c}
	% 1 &    -\sin(\phi) &  0  \\
	%    - \sin(\phi)  & 1 & 0 \\
	% 0 & 0 & 1
	% \end{array}
	% \right]
	% \left[
	% \begin{array}{c}
	% {\bf a}^1 \\ {\bf a}^2 \\ {\bf a}^3
	% \end{array}
	% \right], 
	% \nonumber \\
	% \left[
	% \begin{array}{c}
	% {\bf a}^1 \\ {\bf a}^2 \\ {\bf a}^3
	% \end{array}
	% \right]
	% &=& {\sf T}^{-T}{\sf G}_p\left[
	% \begin{array}{c}
	% {\bf p}_1 \\ {\bf p}_2 \\ {\bf p}_3
	% \end{array}
	% \right] = {\sf T}^{-T}{\sf G}_d\left[
	% \begin{array}{c}
	% {\bf d}_1 \\ {\bf d}_2 \\ {\bf d}_3
	% \end{array}
	% \right]
	% \nonumber \\
	% &=& 
	% \left[
	% \begin{array}{c c c}
	% \sec^2(\phi) &  \sec(\phi)\tan(\phi) &  0  \\
	% \sec(\phi)\tan(\phi)  & \sec^2(\phi)  & 0 \\
	% 0 & 0 & 1
	% \end{array}
	% \right]
	% \left[
	% \begin{array}{c}
	% {\bf a}_1 \\ {\bf a}_2 \\ {\bf a}_3
	% \end{array}
	% \right].
	% \end{eqnarray}
	% Here, the matrix ${\sf T}$ and its inverse ${\sf T}^{-1}$ are
	%  \begin{eqnarray}\label{transforma-matrices}
	% {\sf T}
	% &=&
	% \left[
	% \begin{array}{c c c}
	% \sec(\theta) & 0 & - \tan(\theta)  \\
	% - \sin(\phi)\sec(\theta) & \cos(\phi) & \sin(\phi)\tan(\theta)  \\
	% 0 & 0 & 1
	% \end{array}
	% \right],
	% \nonumber \\
	% {\sf T}^{-1}
	% &=&
	% \left[
	% \begin{array}{c c c}
	% \cos(\theta) & 0 & \sin(\theta)  \\
	% \tan(\phi)  & \sec(\phi) & 0  \\
	% 0 & 0 & 1
	% \end{array}
	% \right].
	% \end{eqnarray}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Angular Velocity and Displacements}\label{app:velocity}

	The angular velocity vector $\bomega$ associated with the 1-2-3 Euler angles has several equivalent representations
	%
	\begin{eqnarray}
	\bomega &=& \dot{\psi}{\bf g}_1 + \dot{\theta}{\bf g}_2
	+ \dot{\phi}{\bf g}_3 
	\nonumber \\
	&=&
	\omega_1{\bf d}_1 + \omega_2{\bf d}_2 + \omega_3{\bf d}_3 
	\nonumber \\
	&=&
	\Omega_1{\bf p}_1 + \Omega_2{\bf p}_2 + \Omega_3{\bf p}_3.
	\end{eqnarray}
	Using (\ref{euler-basis-1-2-3}), it follows that
	\begin{equation}\label{eq:Euler2Gyro}
	\left[\begin{array}{c}
	  \omega_1 \\
	  \omega_2 \\
	  \omega_3
	\end{array}\right] 
	= \left(\mathsf{E}_d\right)^T
	\left[\begin{array}{c}
	 \dot{\psi} \\
	 \dot{\theta} \\
	 \dot{\phi}
	\end{array}\right],
	\qquad
	\left[\begin{array}{c}
	  \omega_1 \\
	  \omega_2 \\
	  \omega_3
	\end{array}\right] 
	= \mathsf{R}
	\left[\begin{array}{c}
	  \Omega_1 \\
	  \Omega_2 \\
	  \Omega_3
	\end{array}\right].
	\end{equation}
	%
	These results are used to relate incremental rotations of the tibia ($\omega_i$) and femur ($\Omega_i$) to changes in the Euler angles and vice versa.

	%%%%%%%%%%%%%%%%%%%%%%

% \subsection{An Element of the Jacobian}\label{app:Dmatrix}
% 	For completeness, the lengthy expression for the matrix $\mathsf{D}$ appearing in the expressions (\ref{jacobian-knee}) and (\ref{inv-jacobian-knee}) for the Jacobian and its inverse is given in this Appendix.
% 	Explicitly,
% 	%
% 	\begin{equation}\label{Dmatrix}
% 	\mathsf{D} = d^{LM}_0 \mathsf{D}_1  + d^{AP}_0 \mathsf{D}_2 + d^{CD}_0 \mathsf{D}_3,
% 	\end{equation}
% 	%
% 	where
% 	%
% 	\begin{eqnarray}
% 	\mathsf{D}_1 &=&
% 	\left[ \begin{array}{c c c}
% 	\sin(\vv)\sin(\ie) & -\tan(\vv)\cos(\ie) & 0 \\
% 	\sin(\vv)\cos(\ie) & - \tan(\vv)\sin(\ie) & 0 \\
% 	0 & \tan(\vv)\left(\tan(\vv) - \sec(\vv)\right) & 0
% 	\end{array}
% 	\right],
% 	\nonumber
% 	\end{eqnarray}
% 	\begin{eqnarray}
% 	\mathsf{D}_2 &=& 
% 	\left[ \begin{array}{c c c}
% 	0 & - \sin(\ie)\cos(\ie)\tan(\vv) & - \cos(2{\ie}) \\
% 	0 & - \sin^2(\ie)\tan(\vv)   &   0 \\
% 	0  & 0 &  0
% 	\end{array}
% 	\right]
% 	\nonumber \\
% 	&&- 
% 	\left[ \begin{array}{c c c}
% 	\sin(\vv ) \sin^2({\ie}) & 0 &  0 \\
% 	\sin(\vv ) \sin({\ie}) \cos({\ie})  & 0  &  0 \\
% 	0 & x &  0
% 	\end{array}
% 	\right]
% 	\nonumber \\
% 	&&+
% 	\left[ \begin{array}{c c c}
% 	\sin(\vv )\cos(\vv ) \cos({\ie}) & 0 & 2 \cos(\vv ) \cos({\ie}) \\
% 	\sin(\vv ) \sin({\ie}) \cos({\vv})  & 0  &  0 \\
% 	\cos^2(\vv) & 0 &  0
% 	\end{array}
% 	\right],
% 	\nonumber
% 	\end{eqnarray}
% 	\begin{equation}
% 	\mathsf{D}_3 = 
% 	\left[ \begin{array}{c c c}
% 	- \cos (\vv) \sin ({\ie} ) & \cos ({\ie} ) & 0\\
% 	 - \cos (\vv ) \cos ({\ie} ) & - \sin ({\ie} )  &  0 \\
% 	 0 & 0 & 0
% 	\end{array}
% 	\right].
% 	\end{equation}
% 	%
% 	In the expression for $\mathsf{D}_2$,
% 	%
% 	\begin{equation}
% 	x = - \sin(\vv ) \sin({\ie})\sec^2(\vv) + \sin(\ie)\tan^2(\vv).
% 	\end{equation}
% 	%
% 	For convenience, we have dropped the superscript $0$ appearing on $\vv$, ${\fext}$, and $\ie$.
	