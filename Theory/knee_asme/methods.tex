%TODO
% talk about anatomical coord frames and the "mechanical axes" of the joint

% standard coord frames / bases (for all joints)
In the analysis of biomechanical joints, it is standard to employ three coordinate frames: $\mathbb{L}$, $\mathbb{P}$, $\mathbb{D}$.
	The first of these frames, which is often known as a laboratory frame, is an inertial reference frame which is associated with a fixed point $O$.
	The second frame, is attached at a point $O_P$ to the proximal anatomical segment and co-rotates with this body.
	Correspondingly, the third frame is attached at a point $O_D$ to the distal anatomical segment and co-rotates with the distal segment.
	The 4 components of these respective frames are denoted by
	%
	\begin{displaymath}
	\mathbb{L} = \left\{O, {\bf E}_1, {\bf E}_2, {\bf E}_3 \right\}, \quad
	\mathbb{P} = \left\{O_P, {\bf p}_1, {\bf p}_2, {\bf p}_3 \right\}, 
	\end{displaymath}
	\begin{equation}
	\mathbb{D} = \left\{O_D, {\bf d}_1, {\bf d}_2, {\bf d}_3 \right\}.
	\end{equation}
	%
	Here, $\left\{ {\bf E}_1, {\bf E}_2, {\bf E}_3 \right\}$,	$\left\{ {\bf p}_1, {\bf p}_2, {\bf p}_3 \right\}$,	and	$\left\{ {\bf d}_1, {\bf d}_2, {\bf d}_3 \right\}$ are right-handed  set of orthonormal vectors.

	% Rotations for body frames
	Of particular interest is the rotation of the distal anatomical segment $\mathcal{S}_D$ with respect to the proximal anatomical segment $\mathcal{S}_P$.
	The rotation can be characterized by a rotation matrix $\mathsf{R}$:
		%
		\begin{equation}
		\left[ \begin{array}{c}
		{\bf d}_1 \\ {\bf d}_2 \\ {\bf d}_3
		\end{array} \right]
		=
		\left[ \begin{array}{c c c}
		R_{11} & R_{12} & R_{13} \\
		R_{21} & R_{22} & R_{23} \\
		R_{31} & R_{32} & R_{33} \\
		\end{array} \right]
		\left[\begin{array}{c}
		{\bf p}_1 \\ {\bf p}_2 \\ {\bf p}_3
		\end{array} \right].
		\end{equation}
		%
		Here, $R_{ik}$ are the components of $\mathsf{R}$.
		We also use the compact notation
		%
		\begin{equation}
		{\bf d}_k = \mathsf{R}{\bf p}_k, \qquad \left(k=1, 2, 3\right).
		\end{equation}
		%
		In this chapter, $\mathsf{R}$ is parameterized by a set of Euler angles $\psi$, $\theta$, and $\phi$.
		Thus, $\mathsf{R}$ is decomposed into the product of a rotation $\psi$ about a unit vector ${\bf g}_1$ followed by a rotation $\theta$ about a unit vector ${\bf g}_2$ and, finally, a rotation $\phi$ about a unit vector ${\bf g}_3$.
		There are 12 possible sets of Euler angles, and, for each set, the first and third angles range from $0$ to $2\pi$.
		Depending on the specific set of Euler angles, the range of the second angle is restricted.
		For the 1-2-3 set of Euler angles used in this chapter
		%
		\begin{equation}
		\theta \in \left(- \frac{\pi}{2}, \frac{\pi}{2}\right).
		\end{equation}
		%
	For each of the 12 sets, the three vectors $\left\{ {\bf g}_1, {\bf g}_2, {\bf g}_3 \right\}$ form a fourth set of basis vectors which is known as the Euler basis.
	This set of basis vectors is not orthogonal (nor is it necessarily right-handed).
	However, ${\bf g}_2$ is always perpendicular to the plane formed by ${\bf g}_1$ and ${\bf g}_3$.


\subsection*{The Dual-Euler Basis}
\label{ssec:dual-euler}
	% dual-Euler basis
	A fifth set of basis vectors, which is known as the dual (or reciprocal) Euler basis $\left\{{\bf g}^1, {\bf g}^2, {\bf g}^3\right\}$, plays a key role in expressing joint moments.
	Given a specific choice of Euler angles, one is able to define the Euler basis  $\left\{ {\bf g}_1, {\bf g}_2, {\bf g}_3 \right\}$.
	The dual-Euler basis is then defined by the following 9 identities:
	%
	\begin{equation}\label{dual-ident}
	{\bf g}^i \cdot{\bf g}_k = 
	\left\{
	\begin{array}{c} 1\,\, \mbox{when}\, \, i = k \\
	0\,\, \mbox{when}\, \, i \ne k \,\,
	\end{array}
	\right\},
	\qquad
	\left(i = 1, 2, 3, k = 1, 2, 3\right).
	\end{equation}
	%
	It is known (see, e.g., \cite{JGS1994}) that the solutions ${\bf g}^i$ to these equations can be represented as follows:
	%
	\begin{displaymath}
	{\bf g}^1 = \frac{1}{g}
	\left({\bf g}_2\times{\bf g}_3\right), 
	\qquad
	{\bf g}^2 = \frac{1}{g}
	\left({\bf g}_3\times{\bf g}_1\right) = {\bf g}_2, 
	\end{displaymath}
	\begin{equation}\label{dual-soln}
	{\bf g}^3 = \frac{1}{g}
	\left({\bf g}_1\times{\bf g}_2\right).
	\end{equation}
	%
	where $g = \left( {\bf g}_1\times{\bf g}_2\right)\cdot{\bf g}_3$.\footnote{For the 1-2-3 set of Euler angles used later in this chapter $g = \cos(\theta)$.} 
	It is important to note that although ${\bf g}_i$ are of unit length by definition, ${\bf g}^i$ are not necessarily of unit length.
	% importance of dual-Euler basis
	As discussed in \cite{OOR06,OOR08}, the dual-Euler basis plays a key role in establishing transparent expressions for conservative moments and constraint moments.
	For instance, if one wishes the restrict the rotation $\phi$ about ${\bf g}_3$, then a constraint moment $M{\bf g}^3$ needs to be applied.
	This moment has no components in the ${\bf g}_1$ or ${\bf g}_2$ directions and so does not affect these rotations.

	% expressing vectors in these bases
	A vector ${\bf b}$ has several distinct representations with respect to the aforementioned bases:
	%
	\begin{equation}
	{\bf b} = \sum_{k=1}^3 B_k {\bf E}_k
	= \sum_{k=1}^3 b_{p_k} {\bf p}_k
	= \sum_{k=1}^3 b_{d_k} {\bf d}_k
	= \sum_{k=1}^3 b^k {\bf g}_k
	= \sum_{k=1}^3 b_k {\bf g}^k .
	\end{equation}
	%
	These representations are all valid, and the components can be obtained by taking the dot product of the vector ${\bf b}$ with the appropriate basis vector.
	For example,
	%
	\begin{equation}\label{components-vectors}
	B_k = {\bf b}\cdot{\bf E}_k, \qquad
	b^k = {\bf b}\cdot{\bf g}^k, \qquad
	b_k = {\bf b}\cdot{\bf g}_k.
	\end{equation}
	%
	However, it is important to distinguish how one computes the component $b^k$ along the basis vector ${\bf g}^k$, because in this case, the dot product ${\bf b}\cdot{\bf g}^k$ is not equivalent to the projection of ${\bf b}$ onto ${\bf g}^k$ (Fig. \ref{fig:vec-components}b).
	This is due to the fact that ${\bf g}^k$ are not necessarily of unit length, unless $b_i = b^k$.

	\begin{figure}[htbp]
		\centering
		\includegraphics[width=1.0\textwidth]{Theory/figs/vec-components.pdf}
		\caption[Comparison of vector components using the Euler and dual-Euler bases]
		{
		Comparison of the components of a vector ${\bf A} = A^1 {\bf g}_1 + A^3 {\bf g}_3 = A_1 {\bf g}^1 + A_3 {\bf g}^3$.
		In the interests of clarity, the $A^2=A_2$ components are assumed to be zero.
		{\bf a}) The components $A_{1,3}$ are equivalent to the projections of ${\bf A}$ onto ${\bf g}_{1,3}$.
		{\bf b}) The components $A^{1,3}$ must be scaled by $g$ to obtain the projections of ${\bf A}$ onto ${\bf g}^{1,3}$, because ${\bf g}^{1,3}$ have a magnitude $g^{-1}$.
			For the 1-2-3 set of Euler angles $g = \cos(\theta)$.
		{\bf c}) To reconstruct the vector ${\bf A}$, the components $A_{1,3}$ are added up along ${\bf g}^{1,3}$, or the components $A^{1,3}$ are added up along ${\bf g}_{1,3}$.
		}
		\label{fig:vec-components}
	\end{figure}

	More commonly in the biomechanics literature, the components $b_k$ are calculated by projecting ${\bf b}$ onto ${\bf g}_k$ (Fig. \ref{fig:vec-components}a), for example in the case of joint moments.
	However it is not always understood that $b_k$ do not add up along ${\bf g}_k$ to produce ${\bf b}$, as they would in the case of an orthogonal basis.
	It should be apparent from Figure \ref{fig:vec-components}c that, in fact, the components $b_k$ add up along the dual-Euler basis vectors ${\bf g}^k$ to produce ${\bf b}$ (the same can be said for $b^k$ along ${\bf g}_k$).

\subsection*{A Joint Coordinate System for the Knee}\label{sect:JCS}
%? Construction of a Functional Coordinate System (FCS) for the Knee Joint
To construct a joint coordinate system for the knee, we identify the proximal segment $\mathcal{S}_P$ with the femur and the distal segment $\mathcal{S}_D$ with the tibia.
	We follow \cite{GS:1983} and use a set of 1-2-3 Euler angles to describe the rotation $\mathsf{R}$ of the tibia relative to the femur.
	For convenience, explicit details on the Euler and dual-Euler basis vectors for this choice of Euler angles are contained in Appendix \ref{app:123-Euler}.\footnote{In particular, expressions for, and graphical representations of, the Euler and dual-Euler basis vectors for the 1-2-3 set of Euler angles are presented in (\ref{euler-basis-1-2-3}) and in Figure \ref{figure-dual-Eulerd}.}

	% euler angles
	The three Euler angles that parameterize this rotation are identified with the three rotational degrees of freedom of the knee joint:
	%
	\begin{eqnarray}
	\mbox{extension-flexion rotation} \,&& \, {\fext} = \psi, \nonumber \\
	\mbox{varus-valgus angle} \,&& \, \vv = \theta, \nonumber \\
	\mbox{internal-external rotation} \,&& \, {\ie} = \phi.
	\nonumber 
	\end{eqnarray} 
	%	
	Commensurate with the choice of Euler angles, the basis $\left\{{\bf g}_1, {\bf g}_2, {\bf g}_3\right\}$ is defined, where the vector ${\bf g}_1 = {\bf p}_1$ is taken to be aligned with the femur-fixed extension-flexion axis, and the vector ${\bf g}_3 = {\bf d}_3$ is aligned with the tibia-fixed internal-external rotation axis (Fig. \ref{figure-knee-joint}a). 
		% importance of floating axis
		A key feature of these axes is that the angle subtended by them is $\frac{\pi}{2}$ minus the varus-valgus rotation angle $\vv$ (which is negative in Figure \ref{figure-knee-joint}b).
		The axis ${\bf g}_2$ associated with the latter angle of rotation points along the varus-valgus axis, which is both parallel to the tibial plateau and perpendicular to the line connecting the two points of contact between the femoral condyles and the tibial plateau.

		\begin{figure}[!htbp]
			\centering
		  \includegraphics[width=0.9\textwidth]{Theory/figs/knees.pdf}
			% \begin{center} \epsfig{file=Figure1.eps,scale = 0.6} \end{center}
			\caption[Bases associated with the knee joint]
			{
			Bases associated with the knee joint, which is modeled as a truncated cone (femur) and plane (tibia).
			{\bf a}) Proximal and distal frames of the femur and tibia, the Euler basis $\left\{ {\bf g}_1, {\bf g}_2, {\bf g}_3\right\}$ associated with rotational motions of this joint, and the dual-Euler basis $\left\{ {\bf g}^1, {\bf g}^2, {\bf g}^3\right\}$.
			{\bf b}) Frontal plane view of the femoral condyles.
			The extension-flexion (EF) axis ${\bf g}_1$ is aligned with the axis of the truncated cone representing the femur.
			The internal-external (IE) rotation axis ${\bf g}_3$ is aligned with the normal of the plane representing the tibia.
			Note that in this figure, $\vv <  0$.
			}
			\label{figure-knee-joint}
		\end{figure} 

	% model
		We model the tibia as a rigid body with a planar articular surface and the femur as a rigid body with a conical articular surface. %? cite?
			The knee joint is assumed to be under compression, so both condyles are engaged and the angle $\theta$ is constrained: $\theta = \theta_0$.
	 		% key features of the model
			Superimposing the Euler basis vectors and dual-Euler basis vectors on an image of the knee joint leads to several interesting observations.
				First, the angle $\theta$ is constant when both condyles are in contact with the tibial plateau.
				% This is evidenced in the results shown in Fig. 3 of [1].
				Second, under the condition of contact by both condyles with the tibial plateau, a contact moment in the ${\bf g}_2$ direction is present.
				This situation is similar to the sliding cylinder discussed in \cite{OOR06}.
				Our final observation is that the vector ${\bf g}^1$ is parallel to the line segment $\overline{C_1C_2}$ connecting the two points of contact between the femur and tibia.
			
			% others
			Others have modeled the two primary rotational degrees of freedom see of the knee using variations of a compound hinge model, \cite{Churchill,Hollister,Asano}.
				However, to the author's knowledge, the cone and plane model featured here is the first to describe the two rotation axes along with additional degrees of translational freedom along the plane.

% in practice
In practice the  femoral and tibial  axes ${\bf g}_1$ and ${\bf g}_3$ can be defined using bony landmarks, and the axis ${\bf g}_2$ can then be defined as the unit vector perpendicular to the plane formed by ${\bf g}_1$ and ${\bf g}_3$.
	For example, it is well-accepted \cite{GS:1983} that the extension-flexion (EF) axis is fixed to the femur and passes through its lateral and medial epicondyles, the internal-external rotation axis is fixed to the tibia and is parallel to its longitudinal axis, and the varus-valgus rotation axis is floating and is perpendicular to both the extension-flexion and internal-external rotation axes.
	However, for a given motion, it is well-known that the choice of the femoral and tibial axes effects the resulting values of the three angles ${\fext}$, $\vv$, and ${\ie}$ (see, e.g., \cite{Most}).
	
	To eliminate some of this variability, optimization schemes have been proposed with the aim of specifying ${\bf g}_1$ and/or ${\bf g}_3$ on the basis of functional motion rather than anatomical landmarks (see \cite{Ehrig:2007,Reichl} and references therein).
		These schemes seek to minimize some objective quantity (e.g., varus-valgus rotation and/or net tibial translation) under a particular set of assumptions (e.g., a compound pinned hinge model, or a shape function for the Euler angles) for a given motion of interest (e.g., flexion within the range of 40\td-80\td, or constrained tibial rotation).
		Motivated by the contact constraint force ${\bf F}_c$ and moment ${\bf M}_c$ described next in Section \ref{force-moments-cond}, we suggest that an optimization scheme be chosen to minimize incremental varus-valgus rotations $\delta\vv$ and compression-distraction translations $\delta d^{CD}$.
		Such an optimization scheme would be valid for any knee joint motion during which ${\bf F}_c$ and ${\bf M}_c$ do not perform work.