\subsection*{Specimen Preparation}
Six fresh-frozen left and right human cadaveric knees were used (4 female, 2 male; average age, 54 years).
Legs with major structural bony and ligamentous abnormalities were not considered.
Specimens were stored at –50\td C and thawed at room temperature for 12 hours before testing.
The tibia and fibula were cut 3 cm proximal to the tibiotalar joint so that the longitudinal axis of the tibia could be accurately localized, and the femur was cut at the neck so that it could be properly potted.
Soft tissue was stripped from the proximal femur, leaving intact tissue 20 cm proximal to the joint line.
The proximal femur was then secured in a custom-made cylindrical container with Smooth Cast 300 (Smooth-On Inc, Easton, Pennsylvania).
The potted specimens were mounted on a base plate that was securely attached to the rigid platform.


\subsection*{Application of MPSD}
The MPSD consists of a single constant-tension spring (48 N) that crosses the knee along the lateral aspect of the leg and applies a reproducible combination of axial compression, valgus torque, and internal torque throughout manual knee flexion (Figure \ref{fig:fig1-setup}) \cite{Sena2011Quantitative}. This spring’s ends are rigidly attached to the femur and tibia using an external fixation system (Synthes, Paoli, Pennsylvania), which allows for precise positioning of the line of force acting across the knee.
With the knee extended, the constant tension spring initially produces an internal rotatory torque of 2.5 N$\cdot$m, valgus torque of 5.5 N$\cdot$m, and axial compression of 38 N.
As the knee is flexed to 60\td, the spring approaches the mid-portion of the knee (from anterior to posterior), reducing the internal torque to approximately 0 N$\cdot$m while maintaining the valgus and axial loads.

During testing, knee kinematics were recorded using the Optotrak (NDI Inc, Waterloo, Canada) navigation system that consists of a 3-aperture position sensor (accuracy, 0.1 mm and 0.1\td), 2 infrared strobing marker clusters attached to the tibia and femur using Schanz screws (Synthes), and a handheld digital probe for registration of reference points.
This system has been previously validated and used in biomechanical studies by our laboratory \cite{Kennedy2011Biomechanical}.
Femoral and tibial origins were defined at the femoral intercondylar notch and center of the tibial plateau; $x$-axes coincided with the longitudinal bone axes; and points on the medial epicondylar eminence and proximal tibial ridge defined the $xz$ and $xy$ planes of the femur and tibia, respectively (Figure \ref{fig:fig1-setup}, inset).
The points at which the constant-tension spring attaches to the tibial and femoral rods were registered to ensure consistent spring placement between each specimen.

To facilitate proper positioning of the constant-tension spring, a series of clamps and rods were attached separately to the lateral side of the tibia and femur using a surgical external fixation system.
Pin clamps with outrigger posts were rigidly secured to the bone using Schanz screws.
Carbon fiber rods were then mounted on the posts using small swivel clamps, which allowed the rods to be positioned freely in space.
Finally, the constant-tension spring was attached to the tibial and femoral rods.
Before the experiment, the 3-dimensional coordinates of the spring attachment points between the rods were digitally registered using the Optotrak handheld probe.
This step was essential, as it allowed for positioning of the spring attachments within 1 cm of a chosen location relative to the tibia and femur.
Precise placement of the spring ensured that forces and moments were applied consistently between tests.

% --------------------
\begin{figure}[!ht]
  \begin{center}
  \includegraphics[width=0.8\textwidth]{mpsd_peds/figs/fig1-setup.pdf}
  \caption[Mechanical pivot-shift device and measurement coordinate system]{
  Mechanical pivot-shift device and measurement coordinate system.
  A constant-tension spring (\textbf{a}) was held rigidly in a predetermined position relative to the tibia and femur by external fixators (\textbf{b}).
  Optotrak smart markers (\textbf{c}) attached to the tibia and femur enabled the tracking of tibiofemoral motion.
  Potted specimens were mounted on a hinged testing base (\textbf{d}) to allow for free rotation in the sagittal plane during manual testing.
  Inset: Coordinate systems (tibia/femur) were constructed by defining an origin (center of tibial plateau/top of intercondylar notch), a long axis (distal tibial shaft/proximal femoral shaft), and a plane point (anterior tibial ridge/lateral femoral epicondyle).
  Using a $Z_F$-$Y$-$X_T$ Euler angle convention, rotations are defined about the fixed $Z_F$ (flexion), floating $Y$ (valgus), and fixed $X_T$ (external) axes for a left knee.
  }
  \label{fig:fig1-setup}
  \end{center}
\end{figure}
% --------------------


\subsection*{Measurement of Knee Kinematics}
As each knee was taken through a range of flexion, $\psi$, the joint configuration was represented as a set of tibial translations $T(\psi) = \{T_x, T_y, T_z\}$ and rotations $R(\psi) = \{R_{\theta}, R_{\phi}\}$ of the tibia relative to a full-extension reference configuration, where $\theta$ and $\phi$ were the abduction/adduction and internal/external rotation angles, respectively.
To quantify the magnitude of the subluxation phase of the pivot-shift event during MPSD loading, anterior displacement (AD) and internal rotation (IR) of the tibia were calculated relative to intact passive flexion.
To quantify the speed of the reduction phase, posterior translational velocity (PTV) and external rotational velocity (ERV) were calculated relative to the femur.
Data were recorded using the Optotrak and processed using custom MATLAB code (MathWorks, Natick, Massachusetts).

The primary outcome variables were maximum AD (AD\tsb{max}), maximum IR (IR\tsb{max}), maximum PTV (PTV\tsb{max}), and maximum ERV (ERV\tsb{max}).
The MPSD tests were performed in triplicate for each knee, with the reconstruction and testing order randomized between knees to eliminate bias.
In addition, between each reconstruction and at the end of testing, knees were taken through passive flexion to ensure that the measured motions in the ACL-deficient knee state were consistent (i.e.\, that the markers were not bumped during the reconstruction procedure).


\subsection*{Surgical Technique}
A medial arthrotomy was made to expose the tibiofemoral joint for digitization of landmarks and for surgical transection of the ACL.
After the MPSD data in the intact state were collected, the ACL was transected and removed from the notch to reduce the chance of impingement.
Care was taken to preserve the posterior cruciate ligament attachments.
Then, after data were collected in the ACL-deficient state, tunnels for each of the reconstructions were drilled in each knee so that only graft fixation needed to be performed between repeated MPSD measurements on each knee.

The all-epiphyseal (AE) and transtibial over-the-top (TT) reconstructions were performed using an autogenous doubled hamstring tendon graft as described previously \cite{Kennedy2011Biomechanical}.
The semitendinosus tendon was deemed to be of adequate quality and size for solitary use.
The average graft size was 8 mm (range: 7-9 mm).
The grafts were prepared by removing excess muscle, placing over a continuous-loop EndoButton (Smith \& Nephew, Andover, Massachusetts), and whip-stitching the proximal ends with free suture.
The iliotibial band (ITB) reconstruction was performed using autogenous iliotibial band tissue \cite{Kocher2005Physeal}.
In all cases, graft fixations were backed up with a post, and grafts were marked with a tissue-marking pen to check against slipping.

The AE reconstruction (Figure \ref{fig:fig2-surgical}a) was performed by drilling epiphyseal tunnels into both the femoral and tibial sides \cite{Anderson2003Transepiphyseal}.
The femoral tunnel was positioned at the anterior aspect of the ACL footprint, and a guide wire was placed using an ACL aiming guide (Arthrex, Naples, Florida) set at 75\td.
Fluoroscopy was used to confirm tunnel position.
Upon confirmation of adequate positioning in the femoral epiphysis, an appropriately sized reamer was used to overream the tunnel.
Tibial tunnel placement was accomplished with a tibial guide positioned with one end centrally in the ACL tibial footprint and the other end in the tibial epiphysis 12 mm distal to the joint line and 15 mm medial to the medial border of the patellar tendon.
Satisfactory positioning was confirmed with a guide wire, and an 8-mm tunnel was reamed.
The EndoButton and graft were passed, and the EndoButton was flipped.
The knee was then flexed to 30\td\ and the graft cycled, tensioned, and secured distally to the tibial metaphysis with a staple (Arthrex) before tying over a post as back-up fixation.

The TT reconstruction (Figure \ref{fig:fig2-surgical}b) utilized a tunnel on the tibial side with an over-the-top configuration on the femoral side \cite{Andrews1994Anterior,Kim1999Anterior}.
The tibial tunnel was placed using a tibial aiming guide set at 50\td\ and positioned in the central portion of the ACL’s tibial footprint and at a point 3 cm distal to the joint line and 6.5 mm medial to the medial border of the patellar tendon.
After correct positioning was confirmed with a guide wire, an 8-mm reamer was used to drill the tunnel.
Careful soft tissue dissection was performed on the femoral side to prepare it for an over-the-top graft position.
The graft was passed through the tibial tunnel to the appropriate overthe-top position, where it was secured on the lateral aspect of the femur with a staple and back-up fixation with the sutures tied over a post.
The knee was then flexed to 30\td, and the graft was tensioned and fixed to the tibial metaphysis with a staple and then tied over a post.

The ITB reconstruction utilized autologous iliotibial band tissue (Figure \ref{fig:fig2-surgical}c) as a physeal-sparing technique as described by Kocher et al \cite{Kocher2005Physeal}.
Using an oblique incision centered over the lateral joint line, the iliotibial band was dissected free proximally and distally, maintaining its attachment at Gerdy's tubercle.
An anterior and posterior incision were made in the iliotibial band at the joint line and extended proximally to the desired graft length.
Graft preparation was completed with a whip-stitch at the free end.
With the knee in 90\td\ of flexion, the graft was secured to the lateral femoral condyle with a staple and then passed into the joint under the intermeniscal ligament.
The graft was then pulled through the notch and placed in an over-the-top position.
The knee was positioned
in 20\td\ of flexion, and the graft was secured to the tibia just distal and medial to the tibial tubercle with a staple and tied over an adjacent post for back-up fixation.

% --------------------
\begin{figure}[!ht]
  \begin{center}
  \includegraphics[width=0.9\textwidth]{mpsd_peds/figs/fig2-surgical.pdf}
  \caption[Physeal-sparing reconstructions]{
  Physeal-sparing ACL reconstruction techniques.
  (A) All-epiphyseal reconstruction.
  The tunnels were placed in the femoral and tibial anterior cruciate ligament (ACL) attachments and drilled to stay within the epiphysis.
  The graft was secured on the femur with an EndoButton and post and on the tibial side with a staple and post.
  (B) Transtibial over-the-top ACL reconstruction.
  The graft was secured on the femoral and tibial sides with a staple and post.
  (C) Iliotibial band ACL reconstruction.
  The graft was secured first on the femur with a staple and then tensioned appropriately and secured to the tibia with a staple and post.
  Figure reprinted with permission from Kennedy et al.\, \cite{Kennedy2011Biomechanical} \textit{Am J Sports Med.} \textcopyright 2011 AOSSM.}
  \label{fig:fig2-surgical}
  \end{center}
\end{figure}
% --------------------


\subsection*{Determination of KSI}
The KSI is an algebraic formula that combines weighted MPSD outcome measures into a single numeric value.
Logistic regression procedures in JMP (version 10, SAS Institute, Cary, North Carolina) were used to calculate probabilities for the categorical response (intact vs ACL injured) as predicted by the set of continuous measurement variables (AD\tsb{max}, IR\tsb{max}, PTV\tsb{max}, and ERV\tsb{max}).
The logistic regression output provides weighting coefficients for each measurement variable plus an intercept term.
The resulting algebraic formula comprises the KSI algorithm, which maps kinematic peak values to a single continuous number between 0 and 100 for knee-assessing stability.
The value 0 corresponds to the intact knee with the lowest kinematic peak values, while 100 corresponds to the deficient knee with the largest kinematic peak values.
The KSI was applied to the outcome measurements obtained from each of the ACL-reconstructed legs to quantify which procedure best matched the intact state.


\subsection*{Statistical Analysis}
An a priori power analysis was performed with a 10\% difference in AD\tsb{max} as a primary outcome variable.
With type-I and type-II error rates of 5\% and 10\%, 5 knees were calculated to be necessary to determine a statistically significant difference between testing states based on previous data.
Statistical analysis was performed using the MATLAB Statistics Toolbox (version 2009a, MathWorks) and JMP.
A repeated-measures analysis of variance (ANOVA) without knee-by-state interaction was used to determine the effects of the different reconstruction techniques on knee kinematics.
Significance was set at p\tl 0.01.
Data are presented as the mean\tpm\ standard deviation.
Repeatability and reproducibility were quantified by calculating the maximum absolute deviations of measured values from trial means and knee means, respectively.
