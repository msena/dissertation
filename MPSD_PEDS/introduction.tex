Injury to the anterior cruciate ligament (ACL) is common in many sports, especially in soccer, football, basketball, and skiing.
In the United States alone, over 125,000 ACL reconstructions are performed annually \cite{Kim2011Increase}.
The incidence of ACL injuries is increasing with a more active population involved in sports and high-risk activities.
As the success of ACL reconstruction has grown, the number of ACL reconstructions in skeletally immature patients has increased as well.
Delay in reconstruction until skeletal maturity is often not an appropriate option, as persistent knee instability has been shown to increase the rate of meniscal injury and chondral damage \cite{Dumont2012Meniscal,Graf1992Anterior,Lawrence2011Degeneration,Millett2002Associated}.

Conventional transphyseal ACL reconstruction has been questioned because of potential growth arrest and angular deformities caused by physeal damage from tunnel drilling \cite{Kocher2002Management}.
To avoid this potential complication, multiple reconstruction options have been described to restore stability in the skeletally immature patient.
All-epiphyseal (AE), transtibial over-the-top (TT), and iliotibial band (ITB) types of ACL reconstruction are the 3 most popular alternatives to the traditional transphyseal technique.
Selection of the proper reconstruction technique is made more challenging by the lack of clinical evidence supporting any of the reconstruction types.
Recent reviews of outcomes after ACL reconstruction for children concluded that the overall evidence supporting management options for pediatric ACL reconstructions is low \cite{Mohtadi2006Managing,Moksnes2012Management} with no consensus on the preferred treatment option (surgical or nonsurgical) or reconstruction type.

We have previously investigated the effect of different pediatric ACL reconstruction techniques on static knee stability under uniaxial loading \cite{Kennedy2011Biomechanical}.
In this prior study, we found that static rotational and translational laxity were reduced by the AE and TT reconstructions to near intact levels and that internal rotation was overconstrained by the ITB reconstruction at greater than 30\td\ of flexion.
However, our loads were applied statically at fixed flexion angles; thus, it was not possible to evaluate the effect of pediatric ACL reconstructions on dynamic knee stability.

Dynamic instability of the knee is difficult to quantify and may not always be eliminated by ACL reconstructions employing traditional techniques \cite{Markolf2010Relationship}.
In a clinical setting, dynamic instability is evaluated using the pivot-shift maneuver \cite{Lane2008Pivot}.
The presence of a positive pivot shift is more predictive (than anteroposterior instability, as assessed by the Lachman test) of the development of osteoarthritis, failure to return to previous level of play, patient-reported instability, and poor subjective and objective outcomes after ACL reconstruction \cite{Kocher2004Relationships,Leitze2005Implications,Lie2007Persistence}.
Unfortunately, the pivotshift maneuver is highly technique dependent and variable among practitioners.
Consequently, it has poor sensitivity \cite{Bull1998PivotShift,Bach1988Pivot} and is difficult to reproduce in a clinical or laboratory setting \cite{Noyes1991Analysis}.
Past biomechanical studies have employed various instrumented devices designed to mimic the rotational instability observed in ACL-deficient knees \cite{Bedi2011Transtibial,Markolf2010Comparison,Markolf2008Simulated,Musahl2011Effect,Musahl2010Effect,Musahl2010Mechanized,Kanamori2000Forces}.
However, in these studies, knee loads were either unknown or applied statically.
Because different joint loading combinations induce distinctive kinematic behaviors, the results and their interpretation are difficult to compare between studies \cite{Markolf2010Relationship,Musahl2010Influence,Musahl2010Mechanized,Pearle2009PivotShift}.

To overcome these limitations, we have recently developed a mechanical pivot-shift device (MPSD) for consistently applying known dynamic forces and moments to the knee \cite{Sena2011Quantitative}.
The objective of the present study was to use the MPSD to extend our prior findings with regard to pediatric ACL reconstruction to include measures of dynamic stability that may be more reflective of clinical outcomes.
Further, we sought to develop a novel knee stability index (KSI) that combines the multiple MPSD outcome measures into a single continuous value between 0 (intact) and 100 (deficient).
We hypothesized that all pediatric reconstructions would restore individual knee stability measures to intact levels and that the KSI would discriminate stability patterns between reconstruction techniques.