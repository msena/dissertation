The purpose of this study was to biomechanically evaluate the stabilizing effect of several pediatric ACL reconstruction techniques using a novel MPSD, which reproducibly mimics a pivot shift through the application of standardized dynamic loads.
All reconstruction techniques tested were able to improve stability measures relative to the ACL deficient state by some degree.
Consistent with our previous findings \cite{Kennedy2011Biomechanical}, the ITB reconstruction was found to overconstrain AD\tsb{max} and IR\tsb{max}.
On average, the TT reconstruction most closely restored AD\tsb{max} and IR\tsb{max} toward intact levels.
However, the TT reconstruction did not significantly reduce ERV\tsb{max} from deficient levels.
On the other hand, the AE reconstruction restored AD\tsb{max} and IR\tsb{max} and also most closely restored PTV\tsb{max} and ERV\tsb{max} toward intact levels.
Further, the AE reconstruction exhibited the lowest KSI, a logistic regression–based linear combination of all 4 measured dynamic knee stability metrics.

Over the previous 2 decades, a strong emphasis has been placed on the benefits of youth participation in sports.
However, with an increase in youth participation, the rate of ACL injury is expected to grow.
In skeletally immature patients, nonoperative management leads to an increased risk of meniscus and cartilage injuries \cite{Dumont2012Meniscal,Graf1992Anterior}, and therefore, surgical reconstruction is often recommended for those patients who wish to remain active in sports.
Although it has not been proven that drilling across the physis will result in angular growth deformities or growth arrest, many surgeons opt for techniques to avoid this potential complication, especially in younger patients \cite{Kocher2002Management}. 
The best techniques for pediatric ACL reconstruction, from a biomechanical standpoint, should restore dynamic stability of the knee and limit the risk of further injury.

We found that the AE, TT, and ITB physeal-sparing reconstructions were each able to improve kinematic measures of rotational and translational stability relative to ACL-deficient knees under dynamic MPSD loading.
Thus, each reconstruction type may successfully restore dynamic stability to some degree.
However, the ITB reconstruction significantly reduced AD and IR beyond intact levels.
The implications of overconstraining the knee in a juvenile population require further study.
These two findings were consistent with our previous evaluation of static knee stability under uniaxial loading conditions \cite{Kennedy2011Biomechanical}.
Another finding was that, although the TT reconstruction most closely restored AD\tsb{max} and IR\tsb{max} toward intact levels, it did not restore PTV\tsb{max} and ERV\tsb{max} by the same degree (to within 6\% vs to within 21\%).
Correspondingly, 4 of the 6 TT-reconstructed knees included in this study exhibited noticeable spikes in PTV and ERV between 35\td\ and 60\td\ of flexion.
The dynamic KSI reflects the above findings, as the KSI for the TT reconstruction was highest (13.3, where 0 = intact and 100 = deficient) of the 3 reconstructions evaluated.

Unlike the TT reconstruction, the AE reconstruction closely restored PTV\tsb{max} and ERV\tsb{max} to within 9\% of intact levels in addition to restoring AD\tsb{max} and IR\tsb{max}.
Accordingly, the KSI value for the AE reconstruction was the lowest (–4.0).
The AE reconstruction is the most anatomically correct in terms of restoring the ACL footprint anatomy and has been shown recently to improve tibiofemoral contact in a cadaveric study \cite{Stonestreet2012AllEpiphyseal}.
As the most anatomic reconstruction, it makes sense in a biomechanical context that the AE reconstruction would restore dynamic knee stability.
This conclusion is supported by previous adult ACL reconstruction studies in which anatomic reconstructions were found to better restore knee stability compared with nonanatomic reconstructions \cite{Kim2011Anatomic,Sohn2009Transitioning}.

Defining clinically relevant and reproducible measures of dynamic knee stability is a major challenge in orthopaedic research.
Clinically, the pivot shift is utilized to assess dynamic stability.
Although it has been correlated with clinical outcomes in multiple studies \cite{Kim2011Anatomic,Ochiai2012Prospective}, the test is poorly sensitive and variable among practitioners \cite{Bull1998PivotShift}. In an effort to make the pivot shift more reliable, researchers have developed various instrumented devices that either mimic the test or produce quantitative outputs \cite{Bedi2011Transtibial,Markolf2010Comparison,Markolf2008Simulated,Musahl2011Effect,Musahl2010Effect,Musahl2010Mechanized,Kanamori2000Forces}.
However, in these studies, knee loads were either applied statically, or they are unknown.
Static loads do not reflect the dynamic nature of the manual pivot-shift test, and unknown loads are not amenable to biomechanical testing.
Therefore, there has been a need for a simple laboratory tool that applies both dynamic and known pivot-shift loads to the knee over a continuous range of flexion angles.

The MPSD is a simple mechanical device composed of 2 external fixation units and a constant-tension spring.
It applies known and reproducible loads that reproduce the complex kinematics of a pivot shift in an ACL-deficient knee \cite{Sena2011Quantitative}.
In this study, the MPSD was able to elucidate differences in clinically relevant kinematics between pediatric ACL reconstruction techniques.
We chose 4 primary variables as our key parameters to study rotational and translational kinematics of the knee.
Two variables quantified displacements of the tibia (AD\tsb{max} and IR\tsb{max}), which characterize the subluxation phase of the pivot shift.
Two variables also quantified velocities of the tibia relative to the femur (PTV\tsb{max} and ERV\tsb{max}), which characterize the reduction phase of the pivot shift.
The choice of these primary outcome variables was based on recent studies that evaluated different components of the pivot shift to determine the factors that are responsible for the instability.
Rotational and translational tibial displacements have been consistently described in other studies of pivot-shift kinematics.
Lane et al. \cite{Lane2008In} found that anterior tibial translation and tibial rotation had a high correlation between examiners and the ACL state in a clinical study of the pivot shift.
Similarly, Yamamoto et al. \cite{Yamamoto2010Comparison} found a high degree of anteroposterior displacement and pivot-shift grade in a clinical study.
On the basis of these results, we used both AD\tsb{max} and IR\tsb{max} to quantify displacements.
We found that the MPSD was able to induce AD and IR repeatability between trials (within 9\% of trial means) and reproducibly between knees (within 28\% of knee means).
Labbe et al. \cite{Labbe2010Feature} performed a principal component analysis on manually applied pivot-shift tests and found that angular velocity accounted for the most variability.
We found that PTV\tsb{max} in the ACL-deficient knee increased more than 2-fold compared with the intact state.
Similarly, ERV\tsb{max} increased significantly in the ACL-deficient state compared with the intact state, although not by such a high amount (48\%).
Although Labbe et al. \cite{Labbe2010Feature} and others \cite{Ahlden2012Clinical,Lopomo2010PivotShift} suggest that acceleration be evaluated as well, we did not analyze accelerations in this study.
Future studies will explore the ability to use acceleration as an additional outcome variable.

A primary limitation of many of the biomechanics studies of knee kinematics is that they take a complex motion such as the pivot shift and analyze the components separately rather than as part of a composite kinematic signature.
A composite signature could provide a better framework for understanding how multiple kinematic variables contribute to overall knee stability.
The algorithm based approach to combine multiple joint motions into a comprehensive score has been described previously in the knee, albeit with different input variables than the ones we used \cite{Labbe2011Objective}.
In this study, we introduced the KSI as a novel method to quantify a complex kinematic motion such as the pivot shift.
The premise behind the KSI is that MPSD measurements provide independently valuable information regarding knee stability.
To form the KSI, stability measurements were weighted (because their discriminatory value is likely not equivalent) and combined into an algebraic formula that produced a single quantitative stability metric that distinguishes the intact knee state from the deficient state.
Although we obtained the best discrimination between intact and deficient states when all 4 kinematic variables were included in the model, we observed a degree of multicollinearity between our 4 kinematic variables.
Additionally, because of the small sample size, the KSI values for the TT, ITB, and AE reconstructions were not significantly different from each other.
Nonetheless, the KSI results were consistent with those from the 4 independent stability measures.
The AE reconstruction appeared to best restore knee stability to the intact state, as it had the lowest KSI value and most closely restored PTV\tsb{max} and ERV\tsb{max}.
In contrast, the TT reconstruction had the highest KSI value and was the least effective in reducing PTV\tsb{max} and ERV\tsb{max} from deficient levels.
Clearly, more studies are needed to confirm the validity of the KSI in a clinical population.
However, we anticipate that it will be a powerful tool for objectively quantifying dynamic knee stability in the future.

There were several limitations to this study.
We used adult specimens to study a pediatric reconstruction technique because of restrictions in obtaining young cadaveric specimens.
There are likely important differences in kinematics between skeletally immature and mature specimens that we could not assess in this study.
However, we hypothesize that the overall kinematic responses to ACL transection and reconstruction are the same in both groups.
Nonetheless, the present results may not be generalizable to all pediatric patients, especially very young patients.
Further study is merited.
Next, we performed time-zero tests that do not take into account factors such as graft changes during healing or secondary stabilizers of the knee.
This is particularly important when considering the ITB reconstruction, which requires a significant amount of soft tissue healing on the surfaces of the tibia and femur.
Another limitation was that the MPSD is a novel technique that has been studied in our laboratory but has not been validated across multiple investigators.
This limitation is true of many of the other current techniques used to study instability of the knee \cite{Markolf2010Comparison,Musahl2010Mechanized}.
The primary functional limitation of the MPSD is that knee flexion is manual, and thus, the flexion rate is not inherently standardized.
As a result, PTV and ERV were more variable than AD and IR.
As PTV and ERV are directly dependent on flexion rate, we will attempt to standardize it in future studies.
Kinematic data were collected at 50 Hz, which is also a minor weakness of this study.
Increasing the sampling rate will improve future velocity estimates and allow us to calculate accelerations.
Graft fixation can be problematic during biomechanical testing.
However, in this study, we used the MPSD to apply less than physiological loads (50 N, 10 N$\cdot$m).
In addition, we doubly fixed the grafts on either side of the knee.
Thus, we saw no evidence of graft loosening or failure during the experimental procedure.
Despite these limitations, the biomechanical results of this study confirm several characteristics of the pediatric ACL reconstruction techniques that have been reported clinically but never tested biomechanically.

In summary, we used a novel MPSD to evaluate the dynamic stability provided by 3 common physeal-sparing ACL reconstruction techniques.
The MPSD applies dynamic forces and moments to cadaveric knees in a reproducible manner.
While all techniques improved knee stability, our data suggest that the AE technique best restores dynamic stability of an ACL-deficient knee.
Although this is an important first step in determining the best treatment option for pediatric ACL reconstruction, the decision is multifactorial and must be based on age, anatomic considerations, and desire to return to play.
Objective clinical outcomes following these reconstruction techniques will help determine the optimal treatment strategy.