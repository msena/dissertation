% \subsection*{Representations of Forces and Moments}\label{sect:force-moment}
% Forces and Moments
It is possible to represent the forces and moments acting on the knee joint using any of the seven sets of basis vectors discussed in Section \ref{sec:kinematics}.
It is particularly convenient to represent forces with respect to the orthogonal distal frame of the tibia $\left\{{\bf p}_1, {\bf p}_2, {\bf p}_3 \right\}$ due to the mutually perpendicular orientations of the lateral-medial, anterior-posterior, and compression-distraction axes. %? out of place? had to mention somewhere
However the corresponding representations for moments on the distal and proximal frames of the tibia and femur are often inconvenient.
	% co and contravariant components
	For the moments acting on the knee joint, of particular interest here are the representations
	%
	\begin{eqnarray}\label{mom-rep}
	{\bf M} &=& M^{1}{\bf g}_1 + M^{2}{\bf g}_2 + M^{3}{\bf g}_3,
	\nonumber \\
	{\bf M} &=& M_{1}{\bf g}^1 + M_{2}{\bf g}^2 + M_{3}{\bf g}^3.
	\end{eqnarray}
	%
	As discussed in Section \ref{ssec:dual-euler}, the components $M_k$ are computed by projecting ${\bf M}$ onto the  Euler basis vectors, while the components $M^k$ are computed by taking the dot product of ${\bf M}$ with the dual-Euler basis vectors.
	Both representations are equally valid, but it is imperative when presenting moment components for a given joint coordinate system to specify which representation one is using.
	
	% Moment transforms
	The relationship between the components $M_k$ and $M^i$ can be found with the help of (\ref{euler-basis-dual-euler-basis}) or, equivalently, by computing ${\bf g}_i\cdot{\bf g}_k$:
	%
		\begin{eqnarray}\label{moment-comp-rels}
		\left[
		\begin{array}{c}
		M^1 \\ M^2 \\ M^3
		\end{array}
		\right]
		&=&
		\left[
		\begin{array}{c c c}
		\sec^2\left(\vv\right) & 0 &  - \frac{\sin\left(\vv\right)}{\cos^2\left(\vv\right)}  \\
		0 & 1 & 0 \\
		- \frac{\sin\left(\vv\right)}{\cos^2\left(\vv\right)} & 0 & \sec^2\left(\vv\right)
		\end{array}
		\right]
		\left[
		\begin{array}{c}
		M_1 \\ M_2 \\ M_3
		\end{array}
		\right],
		\nonumber \\ 
		\phantom{sadf} \nonumber \\
		\left[
		\begin{array}{c}
		M_1 \\ M_2 \\ M_3
		\end{array}
		\right]
		&=&
		\left[
		\begin{array}{c c c}
		1 & 0 &  \sin(\vv)  \\
		0 & 1 & 0 \\
		 \sin(\vv) & 0 & 1
		\end{array}
		\right]
		\left[
		\begin{array}{c}
		M^1 \\ M^2 \\ M^3
		\end{array}
		\right].
		\end{eqnarray}
	%
	A key feature of these identities is that $M_{1,3}$ and $M^{1,3}$ are simply related by the second Euler angle $\vv$.
	For instance, as $|\vv|$ increases from $0$, then the components $M_1$ and $M_3$ become increasingly distinct from $M^1$ and $M^3$.
	Some examples of these relations are shown in Figure \ref{figure-moment-components}.

	\begin{figure}[!htbp]
		\centering 
	  \includegraphics[width=0.6\textwidth]{Theory/figs/moment-components.pdf}
		% \begin{center} \epsfig{file=Figure4.eps,scale=1.25} \end{center}
		\caption[Three representative examples of moments]
		{
		Three representative examples of moments: 
		$M^3{\bf g}_3$,  $M_1{\bf g}^1 = M_1 \sec^2(\vv){\bf g}_1
		- M_1 \sec^2(\vv)\sin(\vv){\bf g}_3$, and
		${\bf T} = T^1{\bf g}_1 + T^3{\bf g}_3
		= \left(T^1 + \sin(\vv)T^3\right){\bf g}^1 + \left(T^3 + \sin(\vv)T^1\right){\bf g}^3$.
		The different representations for the moments were obtained using (\ref{moment-comp-rels}).
		}\label{figure-moment-components}
	\end{figure}	

% Expressing on Euler or dual-Euler
% 	more discussion of advantages
\subsection*{Expressing Joint Moments with the Dual-Euler Basis}
Expressing force vectors and moment vectors as linear combinations of contravariant basis vectors and dual-Euler basis vectors has a long and illustrious history.
However, the basis vectors are often not explicitly mentioned in classical texts and this can often be a source of confusion.
	In the biomechanics literature specifically, the components $M_k$ acting along the dual-Euler basis vectors ${\bf g}^k$ are usually computed, but the basis vectors themselves are often ignored.
		Rather, the Euler basis vectors are emphasized since they correspond to the axes of rotation of the joint and are thus more intuitive and easier to visualize.
	However, as we shall see, consideration for the dual-Euler basis is important when describing the constraints that limit motion of the knee joint.

	To help illustrate the importance of the dual-Euler basis in expressing joint moments, suppose we wish to find the moment vector ${\bf M}$ which will generate a rotation about ${\bf g}_1$ but not about ${\bf g}_2$ or ${\bf g}_3$.
		It follows that ${\bf M}$ must be in the direction of ${\bf g}_2 \times {\bf g}_3$.
		Consequently, the desired moment is in the direction of ${\bf g}^1$.
		More generally, expressing a moment vector ${\bf M}$ using the components $M_k$ implies that ${\bf M}$ is expressed in terms of the dual-Euler basis, i.e., ${\bf M} = M_1{\bf g}^1 + M_2{\bf g}^2 + M_3{\bf g}^3$.
		Each of the components $M_k$ generates a rotation about a single ${\bf g}_k$.
		
		As discussed in \cite{OOR06,OOR08}, ${\bf M} = \sum^3_{k=1} M_k{\bf g}^k$ is also the most natural representation for expressing conservative moments and constraint (reaction) moments because ${\bf M}\cdot{\bomega} =  M_{1}\dot{{\theta}}_{{EF}} + M_{2}\dot{{\theta}}_{{VV}} + M_{3}\dot{{\theta}}_{{IE}}$.

\subsection*{Constraint Moments at the Knee Joint}\label{force-moments-cond}
One important implication of using the Euler and dual-Euler bases is that (if the axes are constructed properly) varous components of joint forces and moments can be interpreted as being supplied by either the soft tissues or bony geometry of the knee.
	For example, the moment supplied by the knee joint in response to the applied moment ${\bf M}$ can be decomposed into a conservative moment ${\bf M}_{con}$ and a constraint moment $M_c{\bf g}_2$, provided by the soft tissues and femoral condyles, respectively: ${\bf M} = {\bf M}_{con} + M_c{\bf g}_2$.
		Here, the moment $M_c{\bf g}_2$ ensures that the varus-valgus angle $\vv$ is constant, while the moment ${\bf M}_{con}$ resists rotations about the two joint axes ${\bf g}_1$ and ${\bf g}_3$ to some degree.

	% Contact force and moment %* discuss constraints
	The most important stabilizing feature of the knee joint is a pair of contact forces exerted by the condyles which prevents the tibial plateau from passing through them.
		% contact moment
		These forces restrict varus-valgus rotation $\vv$ and compression-distraction $d^{CD}$.
		Referring to Figure \ref{figure-knee-constraint}, the resultant of the pair of forces acts antiparallel to the ${\bf g}_3$ direction.
		The pair is equipollent to a resultant force ${\bf F}_c$ and a resultant moment ${\bf M}_c$ acting at point $C$:
		%
		\begin{eqnarray}
		{\bf F}_c = \mu_1 {\bf a}^3 &=& {\bf F}_{c_1} + {\bf F}_{c_2},
		 \nonumber \\
		{\bf M}_c = \mu_2 {\bf g}^2 &=&  \bpi_1\times{\bf F}_{c_1} + \bpi_2\times {\bf F}_{c_2}.
		\end{eqnarray}
		%
		Here, $\bpi_{1}$ and $\bpi_{2}$ are  the respective position vectors of the condyles $C_{1}$ and $C_{2}$ relative to $C$.
		The force ${\bf F}_c$ is an example of a constraint (or normal) force, while ${\bf M}_c$ is an example of a constraint moment.
		The latter serves to prevent rotation in the ${\bf g}_2$ direction.
		As the relative	translational motion in the ${\bf g}_3$ and relative rotational motion in the ${\bf g}_2$ directions are assumed to be zero when both condyles are in contact with the tibia, ${\bf F}_c$ and ${\bf M}_c$ do no work. %? importance of workless constraints?

	\begin{figure}[!htbp]
		\centering 
  	\includegraphics[width=0.7\textwidth]{Theory/figs/knee-constraint.pdf}
		% \begin{center}  \epsfig{file=Figure5.eps,scale=0.65} \end{center}
		\caption[Joint contact forces and their moment at the knee]
		{
		Joint contact forces and their moment at the knee.
		The reaction forces ${\bf F}_{c_1}$ and ${\bf F}_{c_2}$ acting at the 
		condyles shown in (a) are equipollent to a moment 
		${\bf M}_c = \bpi_1\times {\bf F}_{c_1} + \bpi_2\times {\bf F}_{c_2}$ and a force ${\bf F}_c = \mu_1 {\bf g}_3$ where $\mu_1 = \left({\bf F}_{c_1} + {\bf F}_{c_2}\right)\cdot{\bf g}_3$
		 acting at the point $C$.
		}\label{figure-knee-constraint}
	\end{figure} 

	% stiffness matrix %! add in from moment discussion?
	Discussions of the stiffness of the knee joint can be found in the literature.
		For example, Cammarata and Dhaher \cite{Cammarata} and Hsu et al. \cite{Hsu} present experimental measurements of the stiffness of the joint by comparing a varus-valgus rotation with the corresponding varus-valgus moment at 0\td\ of flexion.
		Because of the multi-degree-of-freedom nature of the knee joint, this data constitutes one of the many components of the stiffness matrix of the knee joint and illuminates the difficulties in measuring a complete set of stiffness data for this joint.

		% possible stiffness matrices
		To elaborate further,  it is possible to construct a variety of stiffness matrices for the knee joint using the methods discussed in \cite{MFSOOR}.
		For instance, one such $6\times6$ matrix could relate the ${\bf E}_i$ components of  the increments in ${\bf F}$ and ${\bf M}$ to the increments $\delta {\fext}$, $\delta \vv$, $\delta {\ie}$, $\delta d^{LM}$, $\delta d^{AP}$, $\delta d^{CD}$.
		Alternatively, another stiffness matrix could be constructed relating the ${\bf d}_k$ components of the increments in ${\bf F}$ and ${\bf M}$ to the six increments $\delta \theta^j$ and $\delta d^i$.
		In the interests of brevity we don't present the explicit details here as they are easily inferred from  \cite{MFSOOR}.
		However, one important point to note is that if the knee is loaded so that  increments to compression-distraction and varus-valgus rotation are not possible (i.e., $\delta d^{CD} = 0$ and $\delta\vv = 0$), then it is possible to construct a $4\times4$ stiffness matrix relating the increments in ${\bf F}\cdot{\bf a}_1$, ${\bf F}\cdot{\bf a}_2$, ${\bf M}\cdot{\bf g}_1$ and ${\bf M}\cdot{\bf g}_2$  to $\delta {\fext}$, $\delta {\ie}$, $\delta d^{LM}$, and $\delta d^{AP}$.
		Such a stiffness matrix would not be dominated by the components of the reaction force $F_3{\bf a}^3$ and the reaction moment $M_2{\bf g}^2$ which ensure that the compression-distraction and varus-valgus rotation remain constrained.
		This is a unique feature of the coordinate system proposed in the present paper.

		% conservative moment and stiffness		
		If the stiffness of the knee joint is modeled using a potential energy $U = U(\fext, \ie)$, then it follows that the (conservative) moment ${\bf M}_{con}$ produced by the knee joint has the representation \cite{OOR06}.
		%
		\begin{equation}
			{\bf M}_{con} = -\frac{\partial U}{\partial \fext} {\bf g}^1 - \frac{\partial U}{\partial \ie} {\bf g}^3.
		\end{equation}
		%
		The second partial derivatives of $U$ (e.g., $\frac{\partial^2 U}{\partial \theta^2_*}$) provide the three stiffnesses of the knee joint. 
		Assuming that ${\bf M}_{con} = 0$ when $\fext = \ie = 0$, we find the linear approximation
		%
		\begin{equation}
				{\bf M}_{con} \approx -(k_{11}\fext + k_{12}\ie){\bf g}^1 - (k_{12}\fext + k_{22}\ie){\bf g}^3.
		\end{equation}
		%
		To measure the stiffnesses $k_{11}$, $k_{12}$, and $k_{22}$, it is thus necessary to measure $\fext$ and $\ie$ in addition to the applied moment ${\bf M}$.
		We also recall that ${\bf M} = {\bf M}_{con} + M_c{\bf g}_2$ and so the representation ${\bf M} = \sum^3_{k=1} M_k{\bf g}^k$ is more convenient than ${\bf M} = \sum^3_{i=1} M^i{\bf g}_i$.


\subsection*{Use of the Dual-Euler Basis in Other Works}\label{Forces:others}
% Grood and Fujie forces
It is instructive to compare our work with earlier works on the kinematics and kinetics of the knee joint.
	% Grood and translations
		Starting with work in \cite{GS:1983}, two distinct types of displacements are discussed: clinical displacements $q_i$ and joint translations $S_i$.
		Using the dual-Euler basis, it is straightforward to see  that these displacements are simply related:\footnote{See, in particular, equations (4c), (5a), $\ldots$, (7) in \cite{GS:1983}.
		In their work, ${\bf g}_i$ are denoted by ${\bf e}_i$ and a dual-Euler basis is never mentioned.}
		%
		\begin{equation}
		q_1 {\bf g}^1 + q_2 {\bf g}^2 - q_3 {\bf g}^3 = S_1{\bf g}_1 + S_2{\bf g}_2 + S_3{\bf g}_3.
		\end{equation}
		%
		Unfortunately, the magnitudes of ${\bf g}^1$ and ${\bf g}^3$ are $\sec(\vv)$ and so the magnitudes of the displacements $q_1$ and $q_3$ are difficult to interpret physically.

		Furthermore, since ${\bf g}^3$ is not perpendicular to the tibial plateau when $\vv \ne 0$, natural joint translations would produce nonzero displacements $q_3$,  which might be misinterpreted as unnatural joint compression or distraction.

	% Fujie and forces/moments
	The seminal work on forces and moments at the knee joint is Fujie et al. \cite{Fujie:1996}.
		In certain instances in this work, the forces and moments at the knee joint are expressed in terms of the ${\bf g}^k$ basis.
		Specifically, examining (10) and (11) in \cite{Fujie:1996}, one can interpret their components $f_{LM}$, $f_{AP}$, and $f_{PD}$ as the force components ${\bf F}\cdot{\bf g}_k$, and their components $m_{EF}$, $m_{VV}$, and $m_{IE}$ as the moment components ${\bf M}\cdot{\bf g}_k$, respectively.
		Here, ${\bf g}_k$ are the Euler basis vectors for a 3-1-2 set of Euler angles.
		That is,
		%
		\begin{eqnarray}
		{\bf M} &=& m_{EF}{\bf g}^1 + m_{VV}{\bf g}^2 + m_{IE}{\bf g}^3, 
		\nonumber \\
		{\bf F} &=& f_{LM}{\bf g}^1 + f_{AP}{\bf g}^2 + f_{PD}{\bf g}^3.
		\end{eqnarray}
		%
		Thus, for example, $m_{EF}$ is obtained by projecting ${\bf M}$ onto ${\bf g}_1 = {\bf p}_3$.

		% their misinterpretation by Fujie and Desroches
		Unfortunately, Fujie et al. \cite{Fujie:1996} did not explicitly mention the basis vectors they used when they described the aforementioned forces and moments which may cause confusion.
			For example, it was not clear that the ``proximal-distal" force component $f_{PD}$ acts in the ${\bf g}^3$ direction, which as mentioned earlier, is not necessarily perpendicular to the tibial plateau.
			Thus, $f_{PD}{\bf g}^3$ might be misinterpreted as a workless contact constraint force when in fact it would do work during natural joint translations.
			Further, Fujie et al. refer to the components $M_k$ as ``moments about the axes of the joint coordinate system''.
				If they were referring to $\left\{{\bf g}_1, {\bf g}_2, {\bf g}_3 \right\}$, then this interpretation would be incorrect, because as we have shown, the components $M_k$ are in fact the moments about the axes of the dual-Euler basis $\left\{{\bf g}^1, {\bf g}^2, {\bf g}^3 \right\}$.

		% DEsroche and moment components
			In \cite{DCD2010}, the components $M_k = {\bf M}\cdot{\bf g}_k$ are known as the ``motor torques'' or ``orthogonal projections'', and are clearly distinguished from $M^i = {\bf M}\cdot{\bf g}^i$ which are referred to as the ``nonorthogonal projections''.
			In this respect, Desroches et al. deserve credit for pointing out the distinction between the components ${\bf M}\cdot{\bf g}_k$ and ${\bf M}\cdot{\bf g}^k$.
			However, they champion the use of $M^i$ without utilizing results from the literature on dual basis vectors applied to moment vectors--likely not realizing the usefulness of using the components $M_k$ for describing constraint moments.
