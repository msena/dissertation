\chapter{Conclusion and Future Outlook}
\label{ch:Conclusion}

% why is the topic important (big picture)
Quantitative evaluation of musculoskeletal function is one of the most pressing needs in orthopaedic medicine and physical therapy.
Health care policy changes, an increasingly active population, and the rise of personal health monitoring devices are all increasing the demand for and ability to produce quantitative metrics.
Unfortunately, compared to measures of cardiovascular or metabolic health, measures of musculoskeletal health are far more qualitative and subjective.
Thus, to maximize the value of musculoskeletal care in the future, there is a tremendous need for quantitative health metrics including measures of knee joint function and stability.
	
	% increased demand for quantitative outcome measures
	Recent changes in health care policy have created an increased demand for quantitative measures of health outcomes.
		For example, pay-for-performance policies are emphasizing effectiveness and accountability of care, penalizing errors and rewarding efficiency.
			To comply with these policies, health care providers must make an increased effort to record quantitative metrics for care quality and patient outcomes.
		Physical therapists who wish to bill Medicare are now required to use G-codes and G-code modifiers, respectively, to classify and quantify functional impairments.
			However, the tools needed to produce these quantitative measures of impairment are lacking, so therapists must often resort to making estimates.
		Since insurance companies are now required to provide coverage to all individuals, they are experiencing increased pressure to stop paying for procedures that do not work.
			To identify such procedures (as well as to determine when a patient has returned to `normal') insurance companies are becoming dependent on quantitative health metrics.

	% ageing, yet increasingly acive, population
	Both younger and older individuals are placing greater demands on their joints.
		Increasing numbers of children and adolescents are becoming susceptible to injury by participating in sports.
		The rate of ACL injury and reconstruction has been rising steadily over the past 15 years for individuals below the age of 20. % (to 51 per 100,000 population) 
			However, according to the CDC, more than half of all sports injuries in children are preventable.
			Reliable identification of those who could benefit from preventative training will require better quantitative predictors of injury risk. 
		Meanwhile, the number of hip and knee replacements is growing, and older individuals are demanding to be more active after surgery.
			Early identification of those at risk for degenerative osteoarthritis can dramatically reduce the impact of the disease.
			Doing so will require more sensitive indicators of declining joint function.

	% rise of consumer-available tools for quantitative movement analysis
	The hardware for performing quantitative movement analysis is becoming more pervasive, making it possible to collect musculoskeletal health data on an enormous scale.
		High performance cameras, accelerometers, and gyroscopes are becoming smaller, cheaper, and more integrated into everyday devices.
		For example, the iPhone 6 can natively capture video at frame rates as high as 240 frames per second. %cite apple?
			Smartphones and tablets are used regularly by physical therapists and sports trainers to record patient movements, however quantitative metrics are rarely extracted from the video data.
		Hardware from three-dimensional `depth' cameras like the Microsoft Kinect is now being integrated into laptop computers. %cite CES 2014?
			As studied in Chapter \ref{ch:kinect_methods}, these cameras are capable of tracking human movement without markers, albeit at a lower level of accuracy compared to marker-based motion capture systems.
		Accelerometers and gyroscopes capable of recording 3D motions are appearing in the growing number of wearable devices and fitness trackers.
			Currently data from these sensors is processed to provide crude measures of physical activity such as step counts and sleep duration, but algorithms for calculating more relevant musculoskeletal health metrics will certainly appear in the near future.
		%
		All these devices were once used exclusively in a laboratory setting for conducting movement analysis research.
		Now they are in the possession of everyday consumers.
			This presents in an incredible opportunity, and also a challenge, to make the applications useful to health consumers instead of just researchers.


% why this work is important (include specifics)
Given the context of evolving health policy, an aging population, and advancements in consumer technology, the relevance of the work presented in this dissertation should be apparent.
	Hopefully the tools and techniques developed will not only aid researchers in answering questions about knee stability in the laboratory, but will also directly benefit clinicians and their patients suffering from knee injuries.
	The future development of quantitative outcome measures for musculoskeletal health using new sensor technology will be dependent on continued research on topics like the ones presented in the preceding chapters.

	% knee:
	In Chapter \ref{ch:Theory}, we emphasized the importance of biomechanically accurate descriptions of knee kinematics and kinetics.
		Proper representation of knee joint motion and loads serves as the foundation for quantitative analysis, and is critical for making valid clinical interpretations.
		For example, the Euler basis and dual Euler basis were useful for describing the rotations and moments of the knee joint.
			The relationship between these two bases provided a formal connection between the joint contact moment imposed by two femoral condyles and the kinematic constraint limiting varus valgus rotation.
			This formalism also allowed for a simplified representation of the stiffness matrix for the knee joint, eliminating potentially confounding contributions of the articular geometry to joint stiffness.
		
		Future work should focus on making these mathematical developments clinically applicable.
			Experimental data are needed to show the benefits of using the Euler and dual Euler basis with joint axes defined using functional calibration approaches (rather than the axes being defined using anatomical landmarks).
			Clinical studies should also be performed to link quantitative metrics like varus-valgus angle and joint stiffness coefficients to clinical outcomes data.

	% mpsd: 
	In Chapter \ref{ch:mpsd_methods} we presented a new laboratory tool for quantitatively evaluating dynamic joint stability in cadaveric knees.
		This tool, called the Mechanical Pivot Shift Device (MPSD) applied knee loads that were both dynamic and well-defined to mimic a clinical examination called the pivot-shift test.
			Previous methods of quantifying the pivot-shift employed either poorly defined or static loads.
		In Chapter \ref{ch:mpsd_peds} we used this tool to compare the effectiveness of different pediatric ACL reconstruction techniques for restoring native knee kinematics.
			The sensitivity of the device made it straightforward to determine that all of the ACL-reconstruction techniques tested eliminated the pivot-shift, but that one in particular (the iliotibial band reconstruction) over-constrained the knee in axial rotation.
		
		% future: use and improve
		Future research should use the MPSD to study the effects of soft tissue damage and repair on knee kinematics, and also to improve the MPSD.
			In unpublished pilot studies, we found that the magnitude of the pivot-shift was particularly sensitive to damage of the lateral meniscus.
			We also found that different components of the pivot shift were sensitive to cutting either the anteromedial or posterolateral bundles of the ACL.
			We have also used the MPSD to investigate the effect of damaging capsular connective tissue structures like the anterolateral ligament on knee kinematics.
			Improvements to the MPSD can be made by fine tuning the loads applied by the device to better match those applied during the manual test, although these manual loads have yet to be measured experimentally.
			It might also be possible to produce a clinical version of the MPSD that employs accelerometers and gyros instead of an optical tracking system. %! add clinical device to appendix.

	% kinect:
	In Chapter \ref{ch:kinect_methods} we presented a new marker-based motion capture method that leverages consumer-available 3D cameras like the Microsoft Kinect.
		We demonstrated that, using this method, it was possible to measure the 3D position of landmarks with an accuracy and precision on the order of 1-3 cm, and to do so more reliably than a markerless tracking method.
			Unlike markerless methods, which have not been thoroughly evaluated, the new method presented can be directly compared to standard marker-based motion capture, which has been the paradigm for movement analysis for decades.
		The new method presented represents an ideal intermediate between the expensive multi-camera motion capture systems used for research and the relatively inaccurate marker-less motion capture algorithms used for gesture-based video game control and computer interaction.
		
		% future: get into the hands of practitioners
		Future work should focus on developing a software application for movement analysis that can be used easily by therapists and clinicians.
			The algorithms for extracting joint stability metrics from 3D motion data should be fine tuned and validated for specific functional tasks.
			More advanced modeling approaches for associating skin-mounted markers with rigid-body limb segments may also be employed.
				For example, in unpublished work, a linked kinematic chain model of the body was used to constrain the possible joint motions, and rigid body velocities were calculated using `twists' to aid in tracking marker positions from frame to frame.
			Marker occlusion problems could potentially be solved by merging data from two different depth cameras recording from different angles.
			Last, the next generation Kinect sensor (v2) should be utilized for research.
				This sensor has a higher 2D and 3D resolution than the previous version and the included `skeleton' provides a much more robust representation of human movement, which could benefit marker tracking.
					However, our preliminary work with the Kinect v2 determined that tracking of the knee position using the skeleton is not smooth.

% final summary: tools and techniques for quantitative evaluation of knee stability
In closing, within this dissertation we have presented new experimental tools and analytical techniques for quantitative evaluation of knee joint stability.
	% new math -> put the data into context
	Mathematical techniques featuring the Euler and dual Euler bases enable representation of knee joint motion and loads within the context of the biomechanical constraints imposed by the femoral condyles and connective tissue structures.
	% lab tool -> answer research questions about knee stability
	The mechanical pivot shift device enables one to quantitatively assess, with a high degree of reproducibility, the effects of soft tissue damage or repair on knee dynamic knee joint stability in a cadaveric knee model.
	% clinic tool -> routine quantitative movement analysis
	Finally, marker-based motion capture using the Kinect sensor enables accurate and accessible 3D motion capture for lower extremity movement analysis.
	%
	Hopefully, these contributions will benefit biomechanics researchers, surgeons, and physical therapists in both laboratory and clinical settings who face the increasingly important problem of quantifying knee joint function and stability.
